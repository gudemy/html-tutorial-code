# 23 Graphics

## 228 Section Overview

## 229 Colors

[mdn `<color>`](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value )
- keyword
  - `blue`
- rgb
  - `rgb(0,0,255)`
- rgba
  - `rgba(0,0,255,0.5)`
- hex
  - `#0000ff`
- hsl **Don't use**
  - `hsl(240,100%,50%)`
- hsla
  - `hsla(240, 100%, 50%, .5)`

## 230 Picking Color Combinations - Adobe Color (Kuler)

- [Adobe Color](https://color.adobe.com/create)

## 231 Image Types - RasterBitmap Vector

## 232 Font Awesome

## 233 Font Awesome - Performance

## 234 Font Awesome - Instructions on Use Review

## 235 Font Awesome to SVG - Adobe Illustrator

## 236 Understanding SVG - Basic SVG Shapes

## 237 SVG Terminology

## 238 SVG Path Element

## 239 SVG Path - More Examples

## 240 SVG Stroke Fill

## 241 SVG - Notes Resources

## 242 SVG - Viewbox

## 243 Styling SVGs with Css

## 244 SVG - Symbol Use Elements

## 245 Symbol use CSS

## 246 Symbol with Multiple Paths

## 247 Leave Room For Strokes

## 248 Stroke

## 249 symbol use vs img

## 250 Review
