# 22 Expanding Your Skillset

## 216 Section Overview

## 217 q blockquote cite elements cite attribute

- `q`
  - for short quotations (no paragraph breaks)
  - cite attribute adds a citation - only shows up if you look at the source code
- `blockquote`
  - for long quotations
  - cite attribute adds a citation
- `cite`
  - used to represent a reference to a creative work will show.

## 218 pre samp code kbd abbr elements

- All of the following print out in monospace by default
- `pre`
  - preformatted text will show up on the webpage with the same whitespace as in the source code.
  - still need to use HTML entities
- `samp`
  - element to wrap around output from a computer program
- `code`
  - element to wrap around a fragment of computer code
- `kbd`
  - element for keyboard / user input
- `abbr`
  - abbreviation tag should be used whenever you use an abbreviation.
  - example
    - `<abbr title="Hyper Text Markup Language">HTML</abbr>

## 219 HTML Entities

- [wikipedia HTML Entities](https://en.wikipedia.org/wiki/List_of_XML_and_HTML_character_entity_references)

## 220 Linking Images Bookmarks

- make the img element a child of the anchor (a) element.
  - `<a href="someLinkHere"><img src="imgSourceHere"></a>
- Bookmarks
  - set the href attribute to an id on the page.

## 221 box-shadow Property

- box-shadow property
  - very similar to text-shadow property
  - `box-shadow: x-value y-value spread-value blur-value color

## 222 vertical-align property

## 223 mobile devices - mobile history font-boosting

## 224 mobile devices - meta viewport widthdevice-width

## 225 mobile devices - meta viewport initial-scale1

## 226 meta viewport - picture example

## 227 Review
