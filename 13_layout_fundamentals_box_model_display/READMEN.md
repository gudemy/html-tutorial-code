# Chapter 13 Notes

## Section Overview

- video 120

## Layout Resources Review

- [Learn CSS Layout](https://learnlayout.com/toc.html)
- [A Complete Guide to Flexbox css-tricks.com](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- video 121

## Display Property Review

- Every object has a display property
  - an object is anything that shows up on the page
- display property
  - block vs inline
    - block
      - block a new line
      - takes up the entire line; takes up as much width as possible
      - you can next inside:
        - inline elements
        - block level elements
    - inline
      - does not begin a new line
      - takes up as little width as possible
      - you can nest inside:
        - inline elements
  - the display property may have other values
    - [MDN display property](https://developer.mozilla.org/en-US/docs/Web/CSS/display)
  - primarily following display properties will be used
    - block
    - inline
    - inline-block
    - none
    - flex
- video 122

## Hands-On Exercise - Page of Divs

- video 123

## Hands-On Exercise - Page of Divs - Solution

- video 124

## Hands-On Exercise - Horizontal Menu

- video 125

## Hands-On Exercise - Horizontal Menu - Solution

- video 126

## Taco Shop - Display Property

- `display: inline-block`
  - make sure there is no whitespace characters between the html elements tags to ensure that elements end up next to each other.
- video 127

## Review

- Everything is a box
- inline elements
  - can't have margin and padding on the top and bottom
  - can't have a width and heights set
  - can have margin and padding to the left and right
- rounding and centering
  - border-radius
    - TBLE
    - TB LE
    - T B L E
  - `margin: 0 auto;`
    - TBLE
    - TB LE
    - T B L E
- video 128
