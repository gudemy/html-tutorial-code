# Chapter 12 Structuring Documents Notes

## Section Overview

- Video 104

## Document Structure - Terminology

- Video 105

## Semantic HTML

- Video 106

## Hands-On Exercises - Semantic HTML

- Video 107

## Hands-On Exercises - Semantic HTML - Solutions

- Video 108

## Semantic HTML - header nav main

- Video 109

## Semantic HTML - article section

- Video 110

## Semantic HTML - aside footer

- Video 111

## Semantic HTML - Examples

- Video 112

## Semantic HTML - headings - h1-h6

- Video 113

## Semantic HTML - figure figcaption

- Video 114

## Hands-On Exercises

- Video 115

## Hands-On Exercises - Solutions

- Video 116

## Hands-On Exercises - Learning HTML Elements

- Video 117

## Hands-On Exercises - Learning HTML Elements - My Solutions

- Video 118

## Review

- Video 119
