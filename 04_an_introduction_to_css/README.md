# Notes Chapter 4 An Introduction to CSS

## Video 22 - A Separation of Concerns

* HTML - Structure - semantic HTML
* CSS - Formatting - style, looks, colors, fonts
* Javascript - Functionality - Client side execution

HTML semantic HTML, avoid div-itis.

## Video 23 - CSS Rule-sets

in html header element `<link rel="stylesheet" href="main.css">`

in css add an element / tag selector `header {}`, everything inside the declaration block `{}`, add property `width` value `100%` pairs declarations inside of the declaration block `width: 100%;`

* rule-set
  * selector `header`
  * declaration block `{}`
    * declarations `property: value;`
      * property `width`
      * value `100%`

Full example

```css
header {
    width: 100%;
    height: 100px;
    background-color: red;
}
```

## Video 24 - Linking CSS to HTML

1.  External Style Sheet

    In the html head element.

    ```html
    <link rel="stylesheet" href="main.css">
    ```

    * **Preferred method**
   
1.  Internal Styles

    In the html head element

    ```html
    <style>
        header {
            width: 100%;
            height: 100px;
            background-color: red;
        }
    </style>
    ```

1.  Inline Styles

    Put the styles directly in the tags as an attribute.

    ```html
    <header style="width: 100%;height: 100px;background-color: red;"></header>
    ```

    * **NOT RECOMMENDED**

## Video 25 - Multiple CSS Selectors

-   Multiple Selectors

    The following would change the aside and footer elements in a html document.

    ```css
    aside, footer {
        width: 100%;
        height: 100px;
        background-color: red;
    }
    ```

-   Select Everything Selector

    The `*` symbol selects all elements

    ```css
    * {
        margin: 50px;
    }
    ```