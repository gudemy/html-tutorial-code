# Chapter 14 Layout with Flexbox Notes

## 129 Section Overview

- Flexbox is the defacto standard for layout
- [CSS-Tricks Flexbox Guide](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

## 130 Understanding Flexbox - Container Items

- create a flexbox container
  - all of the immediate children (`>`) of the container are a flex items

## 131 Containers Creating a Flex Container

- all layout has to do with containers
- need to start thinking about everything in containers
- css `display: flex;`

## 132 Container Property flex-wrap

- flex-wrap
  - nowrap
    - default
  - wrap
  - warp-reverse

## 133 Container Property flex-direction (primary-axis cross-axis)

- flex-direction
  - row (default)
  - row-reverse
  - column
  - column-reverse
- primary axis
  - the axis which is parallel to flex-direction's value
  - whatever your flex-direction is set to, that is your primary axis
- cross axis
  - the axis which is perpendicular to the primary axis
- examples:
  - `flex-direction: row;`
    - primary axis: horizontal
    - cross axis: vertical
  - `flex-direction: column;`
    - primary axis: vertical
    - cross axis: horizontal

## 134 Container Property flex-flow

- `flex-flow: row wrap;`
- shorthand for
  - `flex-direction: row;`
  - `flex-wrap: wrap`

## 135 Container Property justify-content

- works on the main primary/main axis
- `justify-content: value;`
  - possible values
    - `flex-start`
    - `center`
    - `flex-end`
    - `space-between`
    - `space-around`

## 136 Container Property align-items

- operates on the cross axis
- `align-items: value;`
  - possible values
    - `flex-start`
    - `flex-end`
    - `center`
    - `stretch`

## 137 Container Property align-content

## 138 Item Property align-self

## 139 Item Property order

## 140 Item Property flex-grow

## 141 Item Property flex-shrink

## 142 Item Property flex-basis

## 143 Item Property flex

## 144 Hands-On Exercise - Flexbox Froggy

## 145 Hands-On Exercises

## 146 Hands-On Exercises - Solutions - 8 Pages

## 147 Hands-On Exercises - Solutions - 6 Pages

## 148 Review
