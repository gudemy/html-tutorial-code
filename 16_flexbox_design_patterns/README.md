# Chapter 16 Flexbox Design Patterns

## 159 Section Overview

- Web Fundamentals website

## 160-162 Googles Flexbox Design Pattern I, II, and III

- main point is to use the following
  - flex container
    - `display: flex;`
    - `flex-flow: row nowrap;`
  - flex container children
    - set `width: 100%` to take up a whole row
    - set `width: <100%` to share a row
- design mobile first and then set breakpoints for larger size screens
  - set breakpoints and link in additional CSS files using media attribute to define a `(min-width: 600px)`

## 163 Hands-On Exercises

- see `01_hands-on-exercises` folder

## 164 Hands-On Exercises - Solutions

## 165 Flexbox Example 1 - Response Menu

## 166 Flexbox Example 2 - Above The Fold

## 167 Flexbox Example 3 - Holy Grail Example

## 168 Flexbox Example 4 - Holy Grail Redux

## 169 Hands-On Exercises

## 170 Review
