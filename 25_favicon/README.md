# 25 Favicon

## 268 Section Overview

## 269 Create An Image

## 270 Generate Favicon

## 271 Generate Favicon - Subfolder

## 272 Generate Favicon - Clears Cache

## 273 Generate Favicon - Compression

## 274 Favicon Essentials

## 275 Full Page Background - Favicon

## 276 Review

- [RealFaviconGenerator.Net]("https://realfavicongenerator.net")
- Clear Cache by setting a version
- should use an image that is at least 600 x 600
- [More Info]("../00_code/046_favicon/06_readme/index.html")
