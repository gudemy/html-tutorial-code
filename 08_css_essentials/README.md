# Chapter 08 CSS Essentials

## Section Overview

## All HTML Tags Attributes All CSS Selector Properties

- All HTML elements
  - [MDN Elements / Tags](https://developer.mozilla.org/en-US/docs/Web/HTML/Element)
  - [MDN Attributes](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes)
- All CSS Selectors
  - [w3schools](http://www.w3schools.com/cssref/css_selectors.asp)
- All CSS Properties
  - [MDN CSS Reference](https://developer.mozilla.org/en-US/docs/Web/CSS/Reference)

## border Property

- CSS Review
  - Rule-Sets
    - Property Selector
      - Declaration Block
        - Declarations
          - Property: value;
- declaring the border property and setting its value to something with a size, type, and color will add a border around the selected property.
- Code located in the [border](border/) folder

## border-radius Property

The border-radius css property's value can be set to create rounded corners.

```css
selector {
  border-radius: 100%;
}
```

## display Property

Each element has a default display value, typically **block** or **inline**. Block take up a line and fill their parent element. Inline do cause a new line.

```css
selector {
  display: block;
  /* display: inline; */
  /* display: inline-block; */
}
```

## padding margin Properties

Padding is the space inside of the element border. Margin is the space outside of the element border.

```css
selector {
  /* top, left, right ,bottom */
  padding: 5px, 5xp, 5xp, 5xp;
  margin: 5px, 5px, 5px, 5xp;
}
```

## Box Model

Everything is a box. Boxes can either build upon each other when their display property is set to block, or next to each other, when their display property is set to inline. Each box also consist of the following: content, padding, border, and margin.

## box-Sizing Property

```css
selector {
  box-sizing: border-box;
}
```

## Review

- HTML 
  - Elements Learned so far
    - html `<html></html`
    - head `<head></head>`
    - body `<body></body>`
    - meta `<meta>`
    - title `<title></title>`
    - paragraph `<p></p>`
    - heading `h1-h6` `<h1></h1>`
    - unordered list `<ul></ul>`
    - ordered list `<ol></ol>`
    - list item `<li></li>`
    - link `<link>`
    - image `<img>`
    - anchor `<a></a>`
    - paragraph `<p></p>`
    - div `<div></div>`
  - Attributes are added in the tag elements to define additional information, such as adding a class. `<p class="small"></p>`
- CSS
  - rule-set
    - selector
      - declaration block
        - declarations
          - property
          - value
  
  ```css
  <!-- rule-set -->
  selector {
    <!-- declaration block is everything between {} -->
    <!-- declaration if a property and a value -->
    <!-- width is a property and 100px is a value -->
    width: 100px;
  }
  ```

  - width `width: 100px;`
  - height `height: 100px;`
  - background-color `background-color: black;`
  - `*` selector - selects everything `* {width: 100px;}`
  - color - changes the text color `color: red;`
  - font-size changes the size of the font to the desired **unit of measurement* `font-size: 50px`
  - display - changes how elements appear
    - inline - elements will be displayed right next to each other `display: inline`
    - block - elements will be displayed on top of each other with a line break between them. `display: block`
    - inline-block - elements will be displayed next to each other. `display: inline-block`
    - none - the element will not be displayed. `display: none`
  - padding - the space between the border and the content.
  - border - the space between the padding and margin.
  - margin - the space outside of the border.
  - box-sizing: border-box makes the size of the element max out to the set width. `box-sizing: border-box`
  - border-radius is used to round the corners of an element. `border-radius: 20%`

- General Knowledge
  - HTTP status codes
    - Developer tools / network
  - Developer tools
    - network tab / throttling
    - right-click / inspect
    - elements tab
      - all of the html
      - all of the css
        - including overridden rules
        - the box-model for each element

- HTML Terminology
  - Opening and Closing tags
  - Self-closing tags
  - Nesting tags
  - Parent / child / ancestor /descendant /sibling
  - Tags vs elements
- CSS Terminology
  - rule-set
    - selector
    - declaration block
      - declarations
        - property
        - value
- Separation of concers
- Link CSS to HTML
  - External **Recommended**
  - Internal **Not Recommended** - can be useful at times
  - Inline **Not Recommended** - ok for quick and dirty testing at times
- Integrated Development Environments
  - WebStorm
  - Atom.io
  - Sublime
  - Dreamweaver
  - Visual Studio Code
- GitHub & VCS
  - "Master" Local Git Repository
  - "Origin/Master" Remote Git Repository
- Using Git
  - Basic Git commands
    - git status
    - git add --all
    - git commit -m "some concise descriptive message about the changes"
    - git push origin master
  - Deleting a repository
- HTML and CSS Naming Conventions
  - Files
    - a-z0-9 no spaces
  - Folders
    - a-z0-9 no spaces
- Keyboard Shortcuts
  - Differ between IDEs but can be extrememly useful to improve workflow
    - Visual Studio Code
      - ctrl + l, ctrl + o - opens html in browser using live server
- [Emmet](https://www.emmet.io)
  - [Emmet cheat sheet](https://docs.emmet.io/cheat-sheet/)
- Tag Attributes
- Relative vs Absolute URLs
- Comments
- Box Model

```html
```

```css
```

## Hands-On Exercises

1. Place a div on a page. Format the div to have a width, height, and background color.
1. Take the code from “Hands-On Exercise (1)” and round the corners of the div.
1. Take the code from “Hands-On Exercise (2)” and give the div a grooved border which is 20px.
1. Create a new HTML document. Place two divs on the HTML document. 
   Format both divs with the following declarations:

   - width: 400px;
   - height: 200px;
   - border: 10px dashed red;
   - margin: 20px auto;
     - TRBL
     - TB   RL
     - T   R   B   L
   Also format the second div with this declaration:
   - box-sizing: border-box;
1. Take the code from “Hands-On Exercise (3)” and give the div a background image by using the following CSS declarations:
  
   - background-image: url("path/to/some/image.png");
   - background-size: cover;
   - background-repeat: no-repeat;

## Hands-On Exercises Solutions
