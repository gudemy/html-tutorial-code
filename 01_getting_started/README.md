# Notes

## Video 1 - Having Fun 1 HTTP Status Codes

The web is made up on computers communicating with each other. The relationship computers have when communicating is called a client server relationship. The clients make request to servers using protocols, frequently they use http.

HTTP - has status codes that the clients and server use to communicate with.

## Video 2 - Having Fun 2 Making The News

Developer tools is a handy way to view and modify pages. In Chrome ctrl+shift+i or right-click inspect.

## Video 3 - Having Fun 3 The Dinosaur Video Game

Dev tools -> Network -> Set online to offline. Press space bar when error page with dinosaur shows up to play a game.

## Video 4 - Websites vs Apps - Should You Learn Web Dev or App Dev

Web has more potential for users.

resources:  
<https://whatwebcando.today>  
<https://moz.com/blog/mobile-web-mobile-apps-invest-marketing-whiteboard-friday>  

## Video 5 - Your Pathway to a $100,000 Year Salary

Look into code bootcamps after taking html/css/javascript/golang courses

## Video 6 - Course Overview - The Art of Building Websites

This course will give all of the tools needed to build a masterpiece.

## Video 7 - Getting The Code Used In This Course

All of the code can be found here: <https://github.com/GoesToEleven/html-css-bootcamp>

## Video 8 - Stay Current Find Jobs Receive Discounts

<https://goo.gl/YJTNed>

## Video 9 - Resources

<https://goo.gl/YJTNed>

## Video 10 - How To Succeed

Focus - Have great focus to achieve, set priority to make sure that happens. No matter what happens make sure you get what you are going for.

Forethought - Make sure you are planting the right seeds and doing the right things each day

7 Habits of Highly Effective People book / audiobook

Time on task - 10,000 hours and having a good teacher.

Grit

"Get in front of what's coming and let it hit you." - Bill Gates
