# Chapter 09 Css Selectors

## Section Overview

## CSS Resets

CSS Resets are to remove the users browser css config to ensure that each user will see the same thing when they visit your page.

## CSS Selectors element class id

- Element selectors

  ```html
  <p class="classname" id="idname"></p>
  ```

  ```css
  p {
      color: red;
  }
  ```

- Class selectors

    ```css
  .classname {
      color: red;
  }
  ```

- Id selectors - IDs can only be used once

  ```css
  #idname {
      color: red;
  }
  ```

## Selector attribute

```html
<a target="_blank" href="https://www.google.com/" contenteditable="true">Editable Text</a>
```

```css
[contenteditable] {
    color: red;
    border: 1px dashed black;
}
```

```css
[target=_blank] {
    color: blue;
    border: 1px dashed black;
}
```

## Selectors pseudo-classes

- LVHA - LiVeHA - live laughter (ha)
  - link
  - visited
  - hover
  - active

```css
a:link {
  color: green;
}
a:visited {
  color: orange;
}
a:hover {
  color: red;
}
a:active {
  color: blue;
}
```

## Selectors pseudo-class focus

Focus is basically the same as active, but it is typically used with forms and input. active is usually used with links / anchor elements.

## Selectors pseudo-class nth child - part I

- `first-child`
- `last-child`
- `nth-child(even)`
- `nth-child(odd)`

## Selectors pseudo-class nth child - part II

- `nth-child(n)`
- `nth-last-child(n)`
- `nth-child(an+b)` - a is the number to increment by and b is the starting location.

  ```css
  li:nth-child(9n+3) {
    color: red;
  }
  ```

- `only-child`

## Selectors pseudo-class typography

pseudo elements

- [`::first-letter`](https://developer.mozilla.org/en-US/docs/Web/CSS/::first-letter)
- `::first-line`

## Selectors nested selectors

- `div p` - all p elements that are a decedent of a div
- `div > p` - all p elements that are an immediate child of div
- `div ~ p` - all p elements that are a sibling that follow a div
- `div + p` - all p elements that are an immediate sibling following a div

## margin 0 auto The Display Property

Set display to block to take up the entire line and then set the margin to the value 0 auto to center the content in the display element.

## Hands-On Exercise - The Surfer Page

## Hands-On Exercise - The Surfer Page - Solution

## Hands-On Exercises

1. Create an html page with an ordered list of 20 list items. Use an ID to select the third list item. Style the third list item red.
1. Create an html page with an ordered list of 20 list items. Use a class to select the third, fourth, and fifth list item. Style these items red.
1. Create an html page with 2 anchor tags providing links to different websites. Give one of the anchor tags the target attribute. Use an attribute selector to select the anchor tag with the target attribute. Style this selected anchor tag to have a border around it.
1. Create an html page with 1 anchor tag. Provide styling for this anchor tag including different colors for these css pseudo-class properties: link, visited, hover, active.
1. Create a paragraph of text. Style the first letter of that paragraph and the first line to be different from the rest of the paragraph.
1. Use this emmet to create this html within an html page’s body tag ...    p{$}+div>p{$$}*3+article>p>lorem^^^p{$$$}*3    … and then, once the html is created, do the following: select all p tags that are children of a div and make them red.
1. Use this emmet to create this html within an html page’s body tag ...    p{$}+div>p{$$}*3+article>p>lorem^^^p{$$$}*3    … and then, once the html is created, do the following: select all p tags that are immediate children of a div and make them red.
1. Use this emmet to create this html within an html page’s body tag ...    p{$}+div>p{$$}*3+article>p>lorem^^^p{$$$}*3    … and then, once the html is created, do the following: select all p tags that are siblings following a div and make them red.
1. Use this emmet to create this html within an html page’s body tag ...    p{$}+div>p{$$}*3+article>p>lorem^^^p{$$$}*3    … and then, once the html is created, do the following: select all p tags that are an immediate sibling following a div and make it red.

## Hands-On Exercises - Solutions I

## Hands-On Exercises - Solutions II

## Review

- CSS resets
- CSS selectors
  - CSS rule sets
    - Selector
      - Declaration Block
        - Declaration
          - Property
          - Value
- CSS Selectors
  - element `element {}`
  - class `.class {}`
  - id `#id {}`
  - attribute `[attribute]`, `selector[attribute=value]`
  - pseudo-class
    - link
    - visited
    - hover
    - active
    - focus
    - nth-child
    - fist-child
    - last-child
    - nth-child(even)
    - nth-child(odd)
    - nth-child(n)
    - nth-last-child(n)
    - nth-child(an+b) `a = num to increment by` `b = the number to start at`
    - only-child
    - first-letter
    - fist-line
  - nested selectors
    - div p
      - all p descendants of div
    - div > p
      - all immediate descendants of div
    - div ~ p
      - all p sibling after div
    - div + p
      - the p sibling right after div
- margin: 0 auto & display
- Surfer Page
- HTML tags learned so far
  - html
  - head
  - body
  - meta
  - title
  - paragraph
  - heading
  - unordered list
  - ordered list
  - list items
  - link
  - image
  - anchor
  - paragraph
  - div
- CSS properties learned so far
  - width
  - height
  - background-color
  - color
  - font-size
  - display
    - display: inline
    - display: block
    - display: inline-block
    - display: none
  - padding
  - border
  - margin
    - margin: 20px auto;
    - margin: 0 auto;
      - TRBL
      - TB RL
      - T R B L
  - box-sizing: border-box
  - border-radius
  - background-image
    - background-image: `url("../path/to/img.jpg")`
    - background-size: cover;
    - background-repeat
      - background-repeat: no-repeat;
  - text-align
    - text-align: center;
  - cursor
    - cursor: pointer;
- CSS selectors learned so far
  - element
  - class
  - id
  - `*`
  - attribute
  - link
  - visited
  - hover
  - active
  - focus
  - first-letter
  - first-line
  - div p
    - all p tags beneath a div
  - div > p
    - all p tags immediately beneath a div
  - div ~ p
    - all p tags that are sibling following a div
  - div + p
    - all p tags that are an immediate sibling following a div
  