# 19 Background

## 189 Section Overview

- `background` is a shorthand for multiple properties related to the background.

## 190 Background Color

- `background-color: value`
  - possible values
    - colors
    - rgb(0-255,0-255,0-255,0-1) - RGBA
    - hex value 00000000 - FFFFFFFF RGBA

## 191 background-image background-repeat background-size

- `background-image`
  - `background-image: url("url/to/an/image.png")`
- `background-repeat`
  - values
    - `no-repeat`
    - `repeat` (default)
    - `repeat-x`
    - `repeat-y`
- `background-size`
  - `<length>`
    - `background-size: 600px 600px;`
  - percentage
  - contain
  - cover

## 192 background-position background-origin

- `background-position`
  - values
    - `center`
    - x,y `300px, 10px`
- `background-origin`
  - `padding-box`
  - `border-box`
  - `content-box`
- `background` shorthand **don't use**

## 193 background-attachment

- `background-attachment`
  - `scroll`
    - background scrolls with the element
  - `fixed`
    - stays in place on the viewport, element leaves it behind when it scrolls

## 194 background

- `background`
- **Do not use**
  - mixed implementation
  - sets missing properties to their initial values
    - can overwrite previously set values
  - some properties share the same value which causes confusion
- It is better to be **CLEAR than to be CLEAVER when programming**

## 195 Hands-On Exercises

## 196 Hands-On Exercises - Solutions

## 197 Review
