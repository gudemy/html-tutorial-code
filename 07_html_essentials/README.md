# Chapter 7 HTML Essentials Notes

## Section Overview

## File Naming Conventions

- All HTML files should end with the .html extension.
- All CSS files should end with the .css extension.
- The default / home web page should be named index.html
- The default stylesheet for your site should be named main.css
- Only use **lowercase** alphanumerics for file names: a-z, 0-9
  - **Do not use spaces in your file names**

## Opening A Project Webstorm

- Only use **lowercase** alphanumerics for folder names: a-z, 0-9
  - **Do not use spaces in your folder names**
- example

    ```mermaid
    graph TD
        A[folder] --> B[css]
        A --> C[img]
        A --> D[js]
        A --> E[pic]
        A --> F(contact.html)
        A --> G(index.html)
        A --> H(services.html)
    ```

## Essential Tags

- Paragraph element
  - p
- Heading elements
  - h1 - h6
- Unordered list element
  - ul
- Ordered list
  - ol
- code
  - [essential_tags.html](code/essential_tags.html "essential_tags.html")

## Emmet.io

[emmet cheat sheet](https://docs.emmet.io/cheat-sheet/ "docs.emmet.io")

h${Header $$$}*6 __tab__

## Modifying Webstorm Formatting

## Tag Attributes

- tags may have attributes
- attributes have a value
  - ["MDN - HTML Basics"](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/HTML_basics "HTML Basics")
- Examples
  - Link element - self-closing
    - ["MDN link element"](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/link "MDN link Element")

    ```html
    <link rel="stylesheet" href="main.css">
    ```

  - Image element - self-closing

    ```html
    <img src="cat.jpeg" alt="a cute cat sitting on a book">
    ```

  - Anchor element

    ```html
    <a href="https://startpage.com" target="_blank">StartPage Search</a>
    ```

  - Paragraph element

    ```html
    <p class="foo">Hello foo bar</p>
    ```

## Relative URLs

- Relative
  - Location of another object with regards to where the current object is.
    - `pic/foo.jpeg`
    - `../css/main.css`
      - `../` mean go up one directory
- Absolute
  - full URL
    - `https://startpage.com`
  - from the root of a site
    - `/index.html`
    - `/pic/cat.png`

## Absolute URLs

## Comments

## Hands-On Exercises

- Challenges
  1. Create two files: an html file and a css file. Name the files using standard naming conventions. Link the css file to the html file.
  1. Take the css file you created in the previous challenge and put that css file in a folder. Use standard naming conventions for the folder. Make sure the index.html file and the main.css file are still linked.
  1. Create this page. Use emmet to build your html structure.
  1. Create this page. Use emmet to build your ordered list and unordered list.
  1. Create this page which demonstrates the use of the image element and the anchor element. Have the anchor element launch a new browser tab.
  1. Add comments to the html document you created in the previous example.
  1. Identify which of these URLs are relative URL’s and which are absolute URLs:
     - www.google.com `absolute`
     - ../pic/dog.jpg `relative`
     - /pic/dog.jpg `absolute`
     - chapter07/index.html `relative`

## Hands-On Exercises - Solutions
