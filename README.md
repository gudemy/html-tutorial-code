# HTML Tutorial

## [Chapter 01 Getting Started](01_getting_started/README.md "Ch01Notes")

## [Chapter 02 Understanding The Internet World Wide Web](02_understanding_the_internet_world_wide_web/README.md "Ch02Notes")

## [Chapter 03 An Introduction to HTML](03_an_introduction_to_html/README.md "Ch03Notes")

## [Chapter 04 An Introduction to CSS](04_an_introduction_to_css/README.md "Ch04Notes")

## [Chapter 07 HTML Essentials](07_html_essentials/README.md "Ch07Notes")

## [Chapter 08 CSS Essentials](08_css_essentials/README.md "Ch08Notes")

## [Chapter 09 CSS Selectors](09_css_selectors/README.md "Ch09Notes")

## [Chapter 10 CSS Order Specificity](10_css_order_specificity/README.md "Ch10Notes")

## [Chapter 11 Formatting Text](11_formatting_text/README.md "Ch11Notes")

## [Chapter 12 Structuring Documents](12_structuring_documents/README.md "Ch12Notes")

## [Chapter 13 Layout Fundamentals - Box Model Display](13_layout_fundamentals_box_model_display/README.md "Ch13Notes")

## [Chapter 14 Layout with Flexbox](14_layout_with_flexbox/README.md "Ch14Notes")

## [Chapter 15 Media Queries](15_media_queries/README.md "Ch15Notes")

## [Chapter 16 Flexbox Design Patterns](16_flexbox_design/README.md "Ch16Notes")

## [Chapter 17 Layout and Position](17_layout_and_position/README.md "Ch17Notes")

## [Chapter 18 Layout with Float](18_layout_with_float/README.md "Ch18Notes")

## [Chapter 19 Background](19_background/README.md "Ch19Notes")

## [Chapter 20 Refactor Challenge](20_refactor_challenge/README.md "Ch20Notes")

## [Chapter 21 Full-Page Above-The-Fold](21_full-page_above-the-fold/README.md "Ch21Notes")

## [Chapter 22 Expanding Your Skillset](22_expanding_your_skillset/README.md "Ch22Notes")

## [Chapter 23 Graphics](23_graphics/README.md "Ch23Notes")

## [Chapter 24 Full-Page Background](24_full-page_background/README.md "Ch24Notes")

## [Chapter 25 Favicon](25_favicon/README.md "Ch25Notes")

## [Chapter 26 Flexbox Practicum - Part 1](26_flexbox_practicum_-_part_1/README.md "Ch26Notes")

## [Chapter 27 Transitions Animations](27_transitions_animations/README.md "Ch27Notes")

## [Chapter 28 Flexbox Practicum - Part 2](28_flexbox_practicum_-_part_2/README.md "Ch28Notes")

## [Chapter 29 You Are A Hero](29_you_are_a_hero/README.md "Ch29Notes")

## [Chapter 30 Forms](30_forms/README.md "Ch30Notes")

## [Chapter 31 Now Go Build It](31_now_go_build_it/README.md "Ch31Notes")

## Resources

- [Course Resources Document](https://goo.gl/YJTNed)
