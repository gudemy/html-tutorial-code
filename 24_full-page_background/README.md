# 24 Full-Page Background

## 251 Section Overview

## 252 Fixed Header

## 253 Fixed Header - Solution

## 254 Header Containers

## 255 Header Containers - Solution

## 256 Make SVGs

## 257 Make SVGs - Solution

## 258 Insert SVGs

## 259 Insert SVGs - Solution

## 260 Style SVGs - Solution

## 261 Dev Tools Styling Company Naming - Solution

## 262 Adding Link Style - Solution

## 263 Responsive Mobile-First

## 264 Responsive Mobile-First - Solution

## 265 Mobile Menu

## 266 Mobile Menu - Solution

## 267 Review
