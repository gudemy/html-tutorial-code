# Notes Chapter 03 An Introduction to HTML

## Video 19 - Your First Webpage

1. <https://codepen.io>
1. <https://emmet.io>

## Video 20 - Anatomy of an HTML Page

1. DOCTYPE - <https://developer.mozilla.org/en-US/docs/Glossary/Doctype>
1. html lang=en
1. head - charset is the encoding typically should be set to UTF-8
1. body - hold the content that will be shown in the browsers view windows.

## Video 21 - HTML Terminology

1. Element vs tags - html, head, body are elements that are represented in code with brackets around them that would be called tags.
1. opening and closing tags
1. self-closing tags
1. nestings tags
1. root is the html tag
