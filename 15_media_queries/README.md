# Chapter 15 Media Queries

## 149 Section Overview

- understanding media queries
  - media type (optional)
  - expression (0 or more)
- media type
  - screen
  - print
- expressions
  - min-width & max-width
- viewport
- breakpoints

## 150 Understanding Media Queries

- Media queries
  - Allow us to apply certain css in certain conditions
  - composed of media type **and/or** a numbe of media features
- Media type (optional)
  - All
  - Screen
  - Print
  - Speech
- Media feature ("expression" - "zero or more")
  - min-width
  - max-width
  - ... and there are more ...
- Examples
  - **In CSS**

    ```css
    @media (min-width: 900px)
    @media (max-width: 900px)
    @ media print
    @ media and (min-width: 900px)
    ```
  
  - In HTML link to stylesheet **preferred method**

    ```html
    <link rel="stylesheet" href="mq-900-plus.css" media="(min-width: 900px)">
    <link rel="stylesheet" href="mq-900-plus.css" media="(max-width: 900px)">
    <link rel="stylesheet" href="mq-900-plus.css" media="print">
    <link rel="stylesheet" href="mq-900-plus.css" media="screen and (min-width: 900px)">
    ```

    - **Preferred Method - HTML link to stylesheet**
      - Allows your code to be more modular
      - Only pulls down the code that is needed for a page
      - with HTTP2, making multiple requests to a server no longer has a negative impact on performance

## 151 Min-Width Max-Width

## 152 Print Media Query

## 153 Media Type Expressions

## 154 Taco Shop Scaling Font - Examples

## 155 Responsive Design Mobile First Design

- Responsive Design
  - Your web page responds to the size of the viewport
- Mobile First
  - Build your pages to look good on mobile first
  - then add in break-points so that your pages work at larger resolutions
- Breakpoints based upon content
  - Create your breakpoint based upon your content
  - Have your content look good, and be readable, at different sizes
- 70 - 80 characters of text per line
  - Classic readability theory suggests that an ideal column should contain 70 to 80 characters per line (about 8 to 10 words in English)
  - when the width of a text block grows past about 10 words, a breakpoint should be considered
- Use
  - **min-width**
  - **max-width**
  - min-height
  - max-height
- Don't use
  - min-device-width
  - max-device-width
  - min-device-height
  - max-device-height

## 156 Hands-On Exercises

## 157 Hands-On Exercises - Solutions

## Review
