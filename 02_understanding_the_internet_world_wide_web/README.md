# Chapter 2 Notes

## Video 11 - History of the Internet

packet-switching could be any route to from the source to the destination.

TCP - Transmission Control Protocol
IP - Internet Protocol

## Video 12 - History of the World Wide Web

Difference between the Internet and the World Wide Web (WWW).
The internet is all of the hardware: cables, routers, servers, etc.
WWW - is a service that runs on top of the internet.

## Video 13 - Who Controls the Internet WWW

No one person or group controls the internet.

## Video 14 - Web Documentation

1. Search "MDN flexbox" MDN is the main.
1. <https://webplatform.org>
1. <https://w3schools.com> DON'T USE
1. <https://caniuse.com>
1. <https://stackoverflow.com>
1. <https://codepen.io> -- misty background experiment
1. <https://csstricks.com> -- A Complete Guide to Flexbox
1. "The Web Ahead" Podcast

## Video 15 - Highlighting Webpages Chrome Extension

Super Simple Highlighter extension
html5 outliner
http headers

## Video 16 - How The Web Works

1. Client / Server architecture. Clients make requests to servers, servers respond to clients. Clients and Servers are both just computers, servers typically just run a different set of software. Most communication is stateless, a request is sent then the server responds depending on the information sent.
1. Request /response pattern - stateless, there is a header and a body in the request and response. Header holds information about the body / payload. Body / payload is the content that the client or server wants to send.

## Video 17 - The Dark Web

1. Tor
2. Jamie Bartlett TED talks

## Video 18 - BitTorrent

P2P file sharing - different architecure than client /server, each person both sends and receives files.
