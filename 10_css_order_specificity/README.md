# Chapter 10 Order Specificity Notes

## Section Overview

- video 081

## The Browsers Application of CSS

- video 082

## Challenge Solution 1

- video 083

## Challenge Solution 2 3 4

- video 084

## Challenge Solution 5 6

- video 085

## Hands-On Exercise

- video 086

## Hands-On Exercise - Solutions

- video 087

## Review

- video 088
- When more than one CSS rule-set is applied to the same element, what determines which element will be applied
  - CSS Specificity
    - inline style
    - id
    - class, attribute, pseudo-class
    - element, pseudo-element
  - Order: last declaration wins
- Compound CSS Selectors
  - Examples:
    - ul#summer-drinks li
    - ul#summer-drinks li.favorite
    - html body ul#summer-drinks li.favorite
