# 17 Layout with Position

## 171 Section Overview

## 172 Layout Essentials - A Quick Reference

[Link](https://goo.gl/k7hbXq)

## 173 Position Overview

## 174 Position Fixed

## 175 Position Relative

## 176 Position Absolute

## 177 Taco Shop - Position Property

## 178 Hands-On Exercises

## 179 Hands-On Exercises - Solutions

## 180 Review
