# 18 Layout with Float

## 181 Section Overview

## 182 Float Images Overflow Auto

- `float: right`
- `float: left`
- The float CSS property specifies that an element should be **taken from the normal flow** and placed along the **left or right side of its container**, where **text and inline elements will wrap around it**.
- If the floated content is larger than its container it will overflow by default.
  - set `overflow: auto` on container to auto grow container to hold the item.

## 183 Float Layout Clearing Floats

- CSS property `clear: value;`
  - values
    - `left`
    - `right`
    - `both`
- Clear will keep an element from sliding under floated content, the element with clear applied to it will respect the floated elements space.

## 184 Float Example - Holy Grail

## 185 Taco Shop - Float Property

## 186 Hands-On Exercises

## 187 Hands-On Exercises - Solutions

## 188 Review
