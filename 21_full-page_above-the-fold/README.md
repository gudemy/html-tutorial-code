# 21 Full-Page Above-The-Fold

## 202 Section Overview

## 203 Step 1 HTML Structure

## 204 Step 1 HTML Structure - Solution

## 205 Step 2 Layout

## 206 Step 2 Layout - Solution

## 207 Step 3 background-image

## 208 Step 3 background-image - Solution

## 209 Step 4 Format Text

## 210 Step 4 Format Text - Solution

## 211 Step 5 Style The Button

## 212 Step 5 Style The Button - Solution

## 213 Step 6 Mobile

## 214 Step 6 Mobile - Solution

## 215 Review
