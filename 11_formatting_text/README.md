# Chapter 11 Formatting Text Notes

## Section Overview

- video 089

## font Property

- video 090
- [MDN font](https://developer.mozilla.org/en-US/docs/Web/CSS/font#Examples)
- CSS properties
  - font
    - font-family
      - a prioritized list of font family names
      - Values
      - `<family-name>`
      - `<generic-name>`
        - serif
        - sans-serif
        - monospace
        - cursive
        - fantasy
    - Serif vs. Sans-Serif
      - Current web design favors sans-serif
      - Article: [Serif vs. Sans: the final battle](http://www.webdesignerdepot.com/2013/03/serif-vs-sans-the-final-battle/)
      - Article: [Serif vs. Sans Serif Fonts: Is One Really Better Than the Other?](https://designshack.net/articles/typography/serif-vs-sans-serif-fonts-is-one-really-better-than-the-other/)
    - font-size
      - Must understand user’s default font-size
        - Usually 16px
        - You can see this in your browser
        - Some people change this
        - font-size values can be based upon user’s default font-size
      - font-size values
        - xx-small, x-small, small, medium, large, x-large, xx-large
          - User’s default font size is medium
          - relative to default font size ( root font size )
        - larger, smaller
          - relative to parent element’s font size
        - `<length>`
          - [MDN length units](https://developer.mozilla.org/en-US/docs/Web/CSS/length#Units)
          - em
            - relative to parent element's font size
          - rem
            - relative to default font size (root font size)
          - vh
            - 1/100th of the height of the viewport
          - vw
            - 1/100th of the width of the viewport
        - `<percentage>`
          - A percentage of the parent element’s font size
          - relative to parent element’s font size
    - font-weight
      - normal
        - same as 400
      - bold
        - same as 700
      - lighter
      - bolder
      - 100, 200, 300, 400, 500, 600, 700, 800, 900
    - font-variant
      - normal
      - small-caps
      - titling-caps
      - unicase
      - [see other values](https://developer.mozilla.org/en-US/docs/Web/CSS/font-variant#Values)
      - Also See: [text-transform](https://developer.mozilla.org/en-US/docs/Web/CSS/text-transform)
        - uppercase
        - lowercase
        - capitalize
    - line-height
      - [MDN line height](https://developer.mozilla.org/en-US/docs/Web/CSS/line-height)
      - On block level elements, the line-height property specifies the minimum height of line boxes within the element
      - **Leading**: space between lines
      - **Kerning**: space between characters
      - values
        - normal
        - `<number>`
        - `<length>`
        - `<percentage>`
    - font-style
      - normal
      - italic
      - oblique

## font-family Property

- video 091
- `font-family: "Some Crazy Font", sans-serif`

## font-size Property

- video 092
- `rem` and `vw` will be used from now on
  - `rem` relative to root front size. Browser default font size setting.
  - `vw` view port width. 1vw = 1/100th of view width
  - `em` relative to parent font size.

## font-weight font-style Properties

- video 093
- used to change the wight or boldness of the font
- font-weight
  - normal `400`
  - bold `700`
  - lighter
  - bolder
  - 100, 200, 300, 400, 500, 600, 700, 800, 900
font-style
  - normal
  - italic
  - oblique
- the font-weight CSS

## font-variant text-transform Properties

- `font-variant: small-caps;`
  - [MDN font-variant](https://developer.mozilla.org/en-US/docs/Web/CSS/font-variant)
- `text-transform: uppercase`
  - [MDN text-transform](https://developer.mozilla.org/en-US/docs/Web/CSS/text-transform)
- video 094

## line-height Property

- `line-height: 2` (leading)
  - values
    - `2`
    - `2em`
    - `normal` ~ 1.2
    - any `<length>` css data type
      - [MDN \<length\>](https://developer.mozilla.org/en-US/docs/Web/CSS/length)
      - [MDN data type](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Types)
  - [MDN line-height](https://developer.mozilla.org/en-US/docs/Web/CSS/line-height)
- video 095

## letter-spacing word-spacing color Properties

- `letter-spacing` (kerning)
  - values
    - normal
    - `<length>` css data type
      - [MDN \<length\>](https://developer.mozilla.org/en-US/docs/Web/CSS/length)
- `word-spacing`
  - values
    - normal
    - `<length>` css data type
      - [MDN \<length\>](https://developer.mozilla.org/en-US/docs/Web/CSS/length)
    - percentage
- `color` - changes the font color
  - values
    - `<color>` css data type
      - [MDN \<color\>](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value)
- video 096

## Google Fonts

- [Google Fonts](https://fonts.google.com/?sort=popularity)
  - Try to stick with most popular fonts to avoid the user having to download more data
  - place link code in html before any of css stylesheets that will define the font-family being used.
- video 097

## text-align text-shadow Properties

- `text-align` - Aligns inline elements within a block level element.
  - the **text-align** CSS property describes how inline content like text is aligned in its parent block element. text-align does not control the alignment of block elements, only their inline content.
  - margin: 0 auto;
  - [MDN text-align](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align)
- `text-shadow` - Adds a shadow to text.
  - the **text-shadow** property adds shadows to text
  - accepts a comma-separated or space list: x, y, blur radius, color
    - offset-x | offset-y | blur-radius | color
    - `text-shadow: 1px 1px 2px black;`
    - [MDN text-shadow](https://developer.mozilla.org/en-US/docs/Web/CSS/text-shadow)
- video 098

## text-decoration Property del

- CSS [`text-decoration`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration) is a shorthand CSS property
  - [text-decoration-line](https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration-line)
    - none
      - often used to remove the underline from anchor `<a>`
    - underline
    - overline
    - line-through
  - [text-decoration-color](https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration-color)
    - `<color>` CSS data type
  - [text-decoration-style](https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration-style)
    - solid
    - double
    - dotted
    - dashed
    - wavy
- HTML
  - [`<del>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/del)
    - **can and maybe should use** `text-decoration: line-through` **instead.**
  - [`<s>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/s)
    - **do not use**
    - use `text-decoration: underline`
- video 099

## text-indent Property

- [MDN `text-indent`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-indent)
  - The **text-indent** property specifies the amount of indentation (empty space) that should be left before lines of text in a block. By default, this controls the indentation of only the first formatted line of the block, but the hanging and each-line keywords can be used to change this behavior. Horizontal spacing is with respect to the left (or right, for right-to-left layout) edge of the containing block element's box.
  - `text-indent: 1rem;`
- video 100

## Hands-On Exercises

1. Use a generic serif font in an HTML document.
1. Use a generic san-serif font in an HTML document.
1. Use a font from Google fonts that has different font-weights. Use the different font-weights.
1. Using CSS, transform all of the text of a sentence to uppercase and italic.
1. Set spacing between characters in a short phrase.
1. Add shadow to the text in a H1 tag.
1. Remove the underline on an anchor tag. And then add the underline in when someone hovers over the link.
1. Show text that is no longer relevant by putting a line through it.
1. Indent the first line of a paragraph

- video 101

## Hands-On Exercises Solutions

- video 102

## Review

- video 103
